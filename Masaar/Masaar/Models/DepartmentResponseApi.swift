//
//    DepartmentResponseApi.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

//    The "Swift - Struct - Gloss" support has been made available by CodeEagle
//    More about him/her can be found at his/her website: https://github.com/CodeEagle

import Foundation
import Gloss

//MARK: - DepartmentResponseApi
public struct DepartmentResponseApi: Glossy {
    
    public let data : [DepartmentData]!
    public let encryptedMessages : [String]!
    public let internalMessages : [String]!
    public let isSucceed : Bool!
    public let messages : [String]!
    public let messagesCode : Int!
    public let serviceDocuments : [String]!
    
    
    
    //MARK: Decodable
    public init?(json: JSON){
        data = "Data" <~~ json
        encryptedMessages = "EncryptedMessages" <~~ json
        internalMessages = "InternalMessages" <~~ json
        isSucceed = "IsSucceed" <~~ json
        messages = "Messages" <~~ json
        messagesCode = "MessagesCode" <~~ json
        serviceDocuments = "ServiceDocuments" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "Data" ~~> data,
            "EncryptedMessages" ~~> encryptedMessages,
            "InternalMessages" ~~> internalMessages,
            "IsSucceed" ~~> isSucceed,
            "Messages" ~~> messages,
            "MessagesCode" ~~> messagesCode,
            "ServiceDocuments" ~~> serviceDocuments,
            ])
    }
    
}
