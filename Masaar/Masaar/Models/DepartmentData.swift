//
//    Data.swift

import Foundation
import Gloss

//MARK: - Data
public struct DepartmentData: Glossy {
    
    public let assistingDocumentURL : [String]!
    public let brief : String!
    public let briefAR : String!
    public let briefEN : String!
    public let briefTrimmed : String!
    public let briefTrimmedAR : String!
    public let briefTrimmedEN : String!
    public let descriptionField : String!
    public let descriptionAR : String!
    public let descriptionEN : String!
    public let detailsURL : String!
    public let eServiceRequestFullURL : String!
    public let eServiceRequestURL : String!
    public let eServiceRequestURLAR : String!
    public let eServiceRequestURLEN : String!
    public let fees : String!
    public let feesAR : String!
    public let feesEN : String!
    public let iD : Int!
    public let imageSrc : String!
    public let mobileImageSrc : String!
    public let parentDepartment : ParentDepartment!
    public let parentService : String!
    public let parentServiceAR : String!
    public let parentServiceEN : String!
    public let parentServiceID : String!
    public let policiesAndProcedures : String!
    public let policiesAndProceduresAR : String!
    public let policiesAndProceduresEN : String!
    public let prerequisites : String!
    public let prerequisitesAR : String!
    public let prerequisitesEN : String!
    public let requiredDocuments : String!
    public let requiredDocumentsAR : String!
    public let requiredDocumentsEN : String!
    public let serviceChannels : String!
    public let serviceChannelsAR : String!
    public let serviceChannelsEN : String!
    public let timeFrame : String!
    public let timeFrameAR : String!
    public let timeFrameEN : String!
    public let title : String!
    public let titleAR : String!
    public let titleEN : String!
    public let titleTrimmed : String!
    public let titleTrimmedAR : String!
    public let titleTrimmedEN : String!
    public let videoID : String!
    public let hasSubServices : Bool!
    public let subServices : [String]!
    
    
    
    //MARK: Decodable
    public init?(json: JSON){
        assistingDocumentURL = "AssistingDocumentURL" <~~ json
        brief = "Brief" <~~ json
        briefAR = "BriefAR" <~~ json
        briefEN = "BriefEN" <~~ json
        briefTrimmed = "BriefTrimmed" <~~ json
        briefTrimmedAR = "BriefTrimmedAR" <~~ json
        briefTrimmedEN = "BriefTrimmedEN" <~~ json
        descriptionField = "Description" <~~ json
        descriptionAR = "DescriptionAR" <~~ json
        descriptionEN = "DescriptionEN" <~~ json
        detailsURL = "DetailsURL" <~~ json
        eServiceRequestFullURL = "EServiceRequestFullURL" <~~ json
        eServiceRequestURL = "EServiceRequestURL" <~~ json
        eServiceRequestURLAR = "EServiceRequestURLAR" <~~ json
        eServiceRequestURLEN = "EServiceRequestURLEN" <~~ json
        fees = "Fees" <~~ json
        feesAR = "FeesAR" <~~ json
        feesEN = "FeesEN" <~~ json
        iD = "ID" <~~ json
        imageSrc = "ImageSrc" <~~ json
        mobileImageSrc = "MobileImageSrc" <~~ json
        parentDepartment = "ParentDepartment" <~~ json
        parentService = "ParentService" <~~ json
        parentServiceAR = "ParentServiceAR" <~~ json
        parentServiceEN = "ParentServiceEN" <~~ json
        parentServiceID = "ParentServiceID" <~~ json
        policiesAndProcedures = "PoliciesAndProcedures" <~~ json
        policiesAndProceduresAR = "PoliciesAndProceduresAR" <~~ json
        policiesAndProceduresEN = "PoliciesAndProceduresEN" <~~ json
        prerequisites = "Prerequisites" <~~ json
        prerequisitesAR = "PrerequisitesAR" <~~ json
        prerequisitesEN = "PrerequisitesEN" <~~ json
        requiredDocuments = "RequiredDocuments" <~~ json
        requiredDocumentsAR = "RequiredDocumentsAR" <~~ json
        requiredDocumentsEN = "RequiredDocumentsEN" <~~ json
        serviceChannels = "ServiceChannels" <~~ json
        serviceChannelsAR = "ServiceChannelsAR" <~~ json
        serviceChannelsEN = "ServiceChannelsEN" <~~ json
        timeFrame = "TimeFrame" <~~ json
        timeFrameAR = "TimeFrameAR" <~~ json
        timeFrameEN = "TimeFrameEN" <~~ json
        title = "Title" <~~ json
        titleAR = "TitleAR" <~~ json
        titleEN = "TitleEN" <~~ json
        titleTrimmed = "TitleTrimmed" <~~ json
        titleTrimmedAR = "TitleTrimmedAR" <~~ json
        titleTrimmedEN = "TitleTrimmedEN" <~~ json
        videoID = "VideoID" <~~ json
        hasSubServices = "hasSubServices" <~~ json
        subServices = "subServices" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "AssistingDocumentURL" ~~> assistingDocumentURL,
            "Brief" ~~> brief,
            "BriefAR" ~~> briefAR,
            "BriefEN" ~~> briefEN,
            "BriefTrimmed" ~~> briefTrimmed,
            "BriefTrimmedAR" ~~> briefTrimmedAR,
            "BriefTrimmedEN" ~~> briefTrimmedEN,
            "Description" ~~> descriptionField,
            "DescriptionAR" ~~> descriptionAR,
            "DescriptionEN" ~~> descriptionEN,
            "DetailsURL" ~~> detailsURL,
            "EServiceRequestFullURL" ~~> eServiceRequestFullURL,
            "EServiceRequestURL" ~~> eServiceRequestURL,
            "EServiceRequestURLAR" ~~> eServiceRequestURLAR,
            "EServiceRequestURLEN" ~~> eServiceRequestURLEN,
            "Fees" ~~> fees,
            "FeesAR" ~~> feesAR,
            "FeesEN" ~~> feesEN,
            "ID" ~~> iD,
            "ImageSrc" ~~> imageSrc,
            "MobileImageSrc" ~~> mobileImageSrc,
            "ParentDepartment" ~~> parentDepartment,
            "ParentService" ~~> parentService,
            "ParentServiceAR" ~~> parentServiceAR,
            "ParentServiceEN" ~~> parentServiceEN,
            "ParentServiceID" ~~> parentServiceID,
            "PoliciesAndProcedures" ~~> policiesAndProcedures,
            "PoliciesAndProceduresAR" ~~> policiesAndProceduresAR,
            "PoliciesAndProceduresEN" ~~> policiesAndProceduresEN,
            "Prerequisites" ~~> prerequisites,
            "PrerequisitesAR" ~~> prerequisitesAR,
            "PrerequisitesEN" ~~> prerequisitesEN,
            "RequiredDocuments" ~~> requiredDocuments,
            "RequiredDocumentsAR" ~~> requiredDocumentsAR,
            "RequiredDocumentsEN" ~~> requiredDocumentsEN,
            "ServiceChannels" ~~> serviceChannels,
            "ServiceChannelsAR" ~~> serviceChannelsAR,
            "ServiceChannelsEN" ~~> serviceChannelsEN,
            "TimeFrame" ~~> timeFrame,
            "TimeFrameAR" ~~> timeFrameAR,
            "TimeFrameEN" ~~> timeFrameEN,
            "Title" ~~> title,
            "TitleAR" ~~> titleAR,
            "TitleEN" ~~> titleEN,
            "TitleTrimmed" ~~> titleTrimmed,
            "TitleTrimmedAR" ~~> titleTrimmedAR,
            "TitleTrimmedEN" ~~> titleTrimmedEN,
            "VideoID" ~~> videoID,
            "hasSubServices" ~~> hasSubServices,
            "subServices" ~~> subServices,
            ])
    }
    
}
