//
//    ParentDepartment.swift

import Foundation
import Gloss

//MARK: - ParentDepartment
public struct ParentDepartment: Glossy {
    
    public let brief : String!
    public let briefAR : String!
    public let briefEN : String!
    public let briefTrimmed : String!
    public let briefTrimmedAR : String!
    public let briefTrimmedEN : String!
    public let iD : Int!
    public let imageSrc : String!
    public let title : String!
    public let titleAR : String!
    public let titleEN : String!
    
    
    
    //MARK: Decodable
    public init?(json: JSON){
        brief = "Brief" <~~ json
        briefAR = "BriefAR" <~~ json
        briefEN = "BriefEN" <~~ json
        briefTrimmed = "BriefTrimmed" <~~ json
        briefTrimmedAR = "BriefTrimmedAR" <~~ json
        briefTrimmedEN = "BriefTrimmedEN" <~~ json
        iD = "ID" <~~ json
        imageSrc = "ImageSrc" <~~ json
        title = "Title" <~~ json
        titleAR = "TitleAR" <~~ json
        titleEN = "TitleEN" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "Brief" ~~> brief,
            "BriefAR" ~~> briefAR,
            "BriefEN" ~~> briefEN,
            "BriefTrimmed" ~~> briefTrimmed,
            "BriefTrimmedAR" ~~> briefTrimmedAR,
            "BriefTrimmedEN" ~~> briefTrimmedEN,
            "ID" ~~> iD,
            "ImageSrc" ~~> imageSrc,
            "Title" ~~> title,
            "TitleAR" ~~> titleAR,
            "TitleEN" ~~> titleEN,
            ])
    }
    
}
