//
//  FacilityController.swift
//  Masaar
//
//  Created by Ramy Nasser Code95 on 7/13/18.
//  Copyright © 2018 Ramy Nasser. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll
import Toast
class FacilityController: UIViewController {

    @IBOutlet weak var noDataLabel: UILabel!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var facilityTableView: UITableView!
    internal var presenter: FacilityPresenter!
    fileprivate var Facilities: [DepartmentData] = []
    fileprivate var pageSize:Int = 10
    fileprivate var pageIndex:Int = 1

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(FacilityController.applyRefresh), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        configurePresenter()
        getFacilities()
    }

    @objc func applyRefresh()  {
        refreshControl.endRefreshing()
        presenter.refreshFacilities(departmentID: 2, pageSize: 10, pageIndex: 1)
    }
    
    
    private func configurePresenter() {
        presenter = Injector.instance.provideFacilityPresenter()
        presenter.setView(view: self)
    }
    
    private func getFacilities()  {
        presenter.getFacilities(departmentID: 2, pageSize: pageSize, pageIndex: pageIndex)
    }

    private func setup() {
        facilityTableView.delegate = self
        facilityTableView.dataSource = self
        facilityTableView.tableFooterView = UIView.init()
        facilityTableView.register(FacilityCell.nib(), forCellReuseIdentifier: FacilityCell.Identifier)
        
        self.title = "Facilities"
        
        applyPagination()
        
        if #available(iOS 10.0, *) {
            facilityTableView.refreshControl = self.refreshControl
            facilityTableView.refreshControl?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        } else {
           facilityTableView.addSubview(refreshControl)
        }
    }

    private func applyPagination() {
        // pagination went here
        facilityTableView.addInfiniteScroll { (tableView) -> Void in
            // update table view
            self.pageIndex = self.pageIndex + 1
            self.presenter.refreshFacilities(departmentID: 2, pageSize: self.pageSize, pageIndex: self.pageIndex)
            // finish infinite scroll animation
            self.facilityTableView.finishInfiniteScroll()
        }
    }

}
extension FacilityController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 172
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let facility = self.Facilities[indexPath.row]
        let router = Injector.instance.provideFacilityRouter(facilityController: self)
        router.navigateToDetails(with: facility)
    }
}
extension FacilityController:UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Facilities.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 20, 0)
        cell.layer.transform = transform
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            cell.layer.transform = CATransform3DIdentity
        }
        let facility = self.Facilities[indexPath.row]
        if let cell = cell as? FacilityCell {
            cell.configureCell(departmentFacility: facility)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: FacilityCell.Identifier, for: indexPath)
     return cell
    }
    
}
extension FacilityController:FacilityPresenterView {
    func showLoading() {
        UiHelpers.showLoader()
    }
    
    func hideLoading() {
        UiHelpers.hideLoader()
    }
    
    func showError(message: String) {
        self.view.makeToast(message)
    }
    
    func showFacilities(_ facilities: [DepartmentData]) {
        self.Facilities.append(contentsOf: facilities)
    }
  
    func refreshFacilities(_ facilities: [DepartmentData]) {
        self.Facilities = facilities
    }
    
    func updateTableView() {
        self.facilityTableView.reloadData()
    }
    
    func hideDataView() {
        self.noDataView.isHidden = true
        self.noDataLabel.isHidden = true
    }
    
    func showDataView() {
        self.noDataView.isHidden = false
        self.noDataLabel.isHidden = false
    }
    
    
}
