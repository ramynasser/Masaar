//
//  FacilityRouter.swift
//  Masaar
//
//  Created by Ramy Nasser Code95 on 7/14/18.
//  Copyright © 2018 Ramy Nasser. All rights reserved.
//

import Foundation
import UIKit

protocol FacilityViewRouter {
    func dismiss()
}
class FacilityRouterImplementation: FacilityViewRouter {
    fileprivate weak var facilityController: FacilityController?
    
    init(facilityController: FacilityController) {
        self.facilityController = facilityController
    }
    
    // MARK: -  characterRouter
    func dismiss() {
        facilityController?.dismiss(animated: true, completion: nil)
    }
    
    func navigateToDetails(with facility:DepartmentData) {
        let detailsVc =  Storyboards.get(.Main).instantiateViewController(withIdentifier: FacilityDetailsController.identifier) as! FacilityDetailsController
        detailsVc.facility = facility
        let detailsVcPresenter = Injector.instance.provideFacilityDetailsPresenter(facility: facility)
        detailsVc.presenter = detailsVcPresenter
        let backButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        facilityController?.navigationController?.navigationItem.backBarButtonItem = backButton
        facilityController?.navigationItem.backBarButtonItem = backButton
        facilityController?.navigationController?.pushViewController(detailsVc, animated: true)
    }
}
