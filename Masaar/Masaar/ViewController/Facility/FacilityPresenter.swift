//
//  FacilityPresenter.swift
//  Masaar
//
//  Created by Ramy Nasser Code95 on 7/13/18.
//  Copyright © 2018 Ramy Nasser. All rights reserved.
//

import Foundation
import Foundation
protocol FacilityPresenterView:class {
    func showLoading()
    func hideLoading()
    func showError(message:String)
    func showFacilities(_ facilities: [DepartmentData])
    func refreshFacilities(_ facilities: [DepartmentData])
    func updateTableView()
    func hideDataView()
    func showDataView()
}

class FacilityPresenter {
    
    fileprivate weak var facilityView : FacilityPresenterView?
    fileprivate let facilityRepository : FacilityRepository
    
    init(facilityRepository: FacilityRepository) {
        self.facilityRepository = facilityRepository
    }
    
    func setView(view : FacilityPresenterView ) {
        facilityView = view
    }
    
    
    func getFacilities(departmentID:Int, pageSize:Int, pageIndex:Int) {
        if UiHelpers.isInternetAvailable() {
            facilityView?.showLoading()
            facilityRepository.getDepartments(departmentID: departmentID, pageSize: pageSize, pageIndex: pageIndex)  { [weak self] (networkReply, statusCode, responseApi) in
                self?.facilityView?.hideLoading()
                    switch(networkReply) {
                    case .success:
                    if let responseData = responseApi?.data {
                           self?.facilityView?.showFacilities(responseData)
                      } else {
                           self?.facilityView?.showFacilities([])
                       }
                        self?.facilityView?.hideDataView()
                        self?.facilityView?.updateTableView()
                   case .failure:
                    self?.facilityView?.showDataView()
                    self?.facilityView?.showError(message: "Something go wrong")
                      }
                   }
                } else {
                self.facilityView?.showDataView()
                self.facilityView?.showError(message: "No internet connection")
            }
    }
    
    func refreshFacilities(departmentID:Int, pageSize:Int, pageIndex:Int) {
        if UiHelpers.isInternetAvailable() {
            facilityRepository.getDepartments(departmentID: departmentID, pageSize: pageSize, pageIndex: pageIndex) { [weak self] (networkReply, statusCode, responseApi) in
                switch(networkReply) {
                case .success:
                    print(statusCode)
                    if let responseData = responseApi?.data {
                        self?.facilityView?.showFacilities(responseData)
                    } else {
                        self?.facilityView?.showFacilities([])
                    }
                    self?.facilityView?.updateTableView()
                case .failure:
                    self?.facilityView?.showDataView()
                    self?.facilityView?.showError(message: "Something go wrong")
                }
            }
        } else {
            self.facilityView?.showDataView()
            self.facilityView?.showError(message: "No internet connection")
        }
    }
    
}
