//
//  TableViewCell.swift
//  Masaar
//
//  Created by Ramy Nasser Code95 on 7/13/18.
//  Copyright © 2018 Ramy Nasser. All rights reserved.
//

import UIKit
import AlamofireImage
import Localize_Swift
class FacilityCell: UITableViewCell {
    
    static let Identifier = "FacilityCell"
    static func nib() -> UINib {
        return UINib(nibName: "FacilityCell", bundle: nil)
    }
    
    @IBOutlet weak var backgroundCardView: CardView!
    @IBOutlet weak var facilityDescriptionLabel: UILabel!
    @IBOutlet weak var facilityTitleLabel: UILabel!
    @IBOutlet weak var facilityImageView: UIImageView!
    
    func configureCell(departmentFacility:DepartmentData) {
        if Localize.isEnglish  {
            self.facilityTitleLabel.text = departmentFacility.titleEN.html2String
            self.facilityDescriptionLabel.text = departmentFacility.briefTrimmedEN.html2String

        } else {
            self.facilityTitleLabel.text = departmentFacility.titleAR.html2String
            self.facilityDescriptionLabel.text = departmentFacility.briefTrimmedAR.html2String
        }
        if let urlString = departmentFacility.mobileImageSrc , let url = URL(string: urlString){
            self.facilityImageView.af_setImage(withURL: url)
        }
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundCardView.layer.cornerRadius = 8.0
        backgroundCardView.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
