//
//  FacilityDetailsController.swift
//  Masaar
//
//  Created by Ramy Nasser Code95 on 7/14/18.
//  Copyright © 2018 Ramy Nasser. All rights reserved.
//

import UIKit
import Localize_Swift
class FacilityDetailsController: UIViewController {
    
    static let identifier = "FacilityDetailsController"
    @IBOutlet weak var scrollView: UIScrollView!
    internal var facility:DepartmentData!
    internal var presenter: FacilityDetailsPresenter!

    @IBOutlet weak var heightConstraint: UIImageView!
    @IBOutlet weak var faciliyImg: UIImageView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var prerequisitesTitleLbl: UILabel!
    @IBOutlet weak var prerequisitesValueLbl: UILabel!
    @IBOutlet weak var requiredDocumentTitleLbl: UILabel!
    @IBOutlet weak var requiredDocumentValueLbl: UILabel!
    @IBOutlet weak var feesValueLbl: UILabel!
    @IBOutlet weak var feesTitleLbl: UILabel!
    @IBOutlet weak var timeFrameTitleLbl: UILabel!
    @IBOutlet weak var timeFrameValueLbl: UILabel!
    @IBOutlet weak var serviceValueLbl: UILabel!
    @IBOutlet weak var serviceTitleLbl: UILabel!
    
    @IBOutlet weak var heightConstriantDescription: NSLayoutConstraint!
    @IBOutlet weak var serviceConstaintHeight: NSLayoutConstraint!
    @IBOutlet weak var timeFrameConstraint: NSLayoutConstraint!
    @IBOutlet weak var feesHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var requiredDocumentConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightConstaintPrerequisitesValueConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightContentConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
         setupNavBar()
    }
    private func setup() {
        if let passedFacility = self.facility {
            if let urlString = passedFacility.mobileImageSrc
                , let url = URL(string: urlString) {
                faciliyImg.af_setImage(withURL: url)
            }
            if Localize.isEnglish {
            descriptionLbl.text = passedFacility.briefEN.html2String
            prerequisitesValueLbl.text = passedFacility.prerequisitesEN.html2String
            requiredDocumentValueLbl.text = passedFacility.requiredDocumentsEN.html2String
            feesValueLbl.text = passedFacility.feesEN.html2String
            timeFrameValueLbl.text = passedFacility.timeFrameEN.html2String
            serviceValueLbl.text = passedFacility.serviceChannelsEN.html2String
                
            } else {
            descriptionLbl.text = passedFacility.briefAR.html2String
            prerequisitesValueLbl.text = passedFacility.prerequisitesAR.html2String
            requiredDocumentValueLbl.text = passedFacility.requiredDocumentsAR.html2String
            feesValueLbl.text = passedFacility.feesAR.html2String
            timeFrameValueLbl.text = passedFacility.timeFrameAR.html2String
            serviceValueLbl.text = passedFacility.serviceChannelsAR.html2String
            }
            prerequisitesTitleLbl.text = "prerequisitesTitle".localized()
            requiredDocumentTitleLbl.text = "requiredDocumentTitle".localized()
            feesTitleLbl.text = "feesTitle".localized()
            timeFrameTitleLbl.text = "timeFrameTitle".localized()
            serviceTitleLbl.text = "serviceTitleTitle".localized()
   
        }
       
    }
    private func setupNavBar() {
        navigationController?.navigationBar.setStyle(style: .solidNoShadow, tintColor: .white, forgroundColor: #colorLiteral(red: 0, green: 0.3568627451, blue: 0.2941176471, alpha: 1))
        if let facility = self.facility {
            if Localize.isEnglish  {
                self.title = facility.titleEN
            } else {
                self.title = facility.titleAR
            }
        }
        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "back")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }

   

}
