//
//  FacilityDetailsPresenter.swift
//  Masaar
//
//  Created by Ramy Nasser Code95 on 7/14/18.
//  Copyright © 2018 Ramy Nasser. All rights reserved.
//

import Foundation
import Foundation
import Foundation
protocol FacilityDetailsView:class {
    func showLoading()
    func hideLoading()
    func showError(message:String)
}

class FacilityDetailsPresenter {
    
    fileprivate weak var facilityView : FacilityDetailsView?
    fileprivate var facility:DepartmentData?
    
  
    init(facility: DepartmentData) {
        self.facility = facility
    }
    
    func setView(view : FacilityDetailsView ) {
        facilityView = view
   }
}
