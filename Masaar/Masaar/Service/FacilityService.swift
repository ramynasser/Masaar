//
//  FacilityService.swift
//  Masaar
//
//  Created by Ramy Nasser Code95 on 7/13/18.
//  Copyright © 2018 Ramy Nasser. All rights reserved.
//
import Foundation
import Moya
import Gloss
enum FacilityService {
    case getDepartmentServices(departmentID:Int, pageSize:Int, pageIndex:Int)
    
}
extension FacilityService:TargetType {
    var baseURL: URL {
        return URL(string: LocalSettings.instance.getURL())!
    }
    
    var path: String {
        switch self {
        case .getDepartmentServices:
            return "/GetDepartmentServices"
        }
    }
    
    var method: Moya.Method  {
        return .post
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestParameters(parameters: self.parameters!, encoding: self.parameterEncoding)
    }
    
    var headers: [String : String]? {
        return LocalSettings.instance.getHeaders()
    }
    
    var parameters: [String: Any]? {
        switch  self {
        case .getDepartmentServices(let departmentID,let pageSize,let pageIndex):
            let localSetting = LocalSettings.instance
            return ["departmentID":departmentID,"pageIndex":pageIndex,"PageSize":pageSize]
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
}
