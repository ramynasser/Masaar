//
//  ApiMarvelRepository.swift
//  MarvelAPIDemo
//
//  Created by Ramy Nasser Code95 on 6/29/18.
//  Copyright © 2018 Ramy Nasser Code95. All rights reserved.
//

import Foundation
import Moya
import Moya_Gloss

typealias DepartmentHandler = (NetworkReply, Int, DepartmentResponseApi?) -> Void

protocol FacilityRepository : class {
    func getDepartments(departmentID:Int, pageSize:Int, pageIndex:Int, completion: @escaping DepartmentHandler)
}
class ApiFacilityRepository: FacilityRepository {
    private var provider :MoyaProvider<FacilityService>!
    
    init() {
        let endpointClosure = { (target: FacilityService) -> Endpoint<FacilityService> in
            let defaultEndpoint = MoyaProvider.defaultEndpointMapping(for: target)
            let header = LocalSettings.instance.getHeaders()
            return defaultEndpoint.adding(newHTTPHeaderFields: header)
        }
        provider = MoyaProvider<FacilityService>(endpointClosure: endpointClosure)
    }
    
    func getDepartments(departmentID:Int, pageSize:Int, pageIndex:Int, completion: @escaping DepartmentHandler) {
        provider.request(.getDepartmentServices(departmentID:departmentID, pageSize:pageSize, pageIndex:pageIndex)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let responseApi = try response.mapObject(DepartmentResponseApi.self)
                    completion(.success, response.statusCode, responseApi)
                } catch {
                    completion(.failure, 400,nil)
                }
            case .failure( _):
                completion(.failure, 404,nil)
            }
        }
    }
    
    
}
