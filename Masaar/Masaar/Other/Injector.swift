//
//  Injector.swift
//  Masaar
//
//  Created by Ramy Nasser Code95 on 7/13/18.
//  Copyright © 2018 Ramy Nasser. All rights reserved.
//

import Foundation
class Injector {
    static let instance : Injector = Injector()
    
    //MARK:- this injector for Repository
    func provideFacilityRepository() -> ApiFacilityRepository {
        return ApiFacilityRepository()
    }
    
    //MARK:- this injector for presenter
    func provideFacilityPresenter() -> FacilityPresenter {
        return FacilityPresenter(facilityRepository:Injector.instance.provideFacilityRepository())
        //(charactersGateway: Injector.instance.provideMarvelRepository())
    }
    
    func provideFacilityDetailsPresenter(facility:DepartmentData) -> FacilityDetailsPresenter {
        return FacilityDetailsPresenter(facility: facility)
    }
    
    //MARK:- this injector for router
    func provideFacilityRouter(facilityController:FacilityController) -> FacilityRouterImplementation {
        return FacilityRouterImplementation(facilityController: facilityController)
    }
    
 
}
