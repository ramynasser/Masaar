//
//  extension + UINavigationBar.swift
//  Masaar
//
//  Created by Ramy Nasser Code95 on 7/14/18.
//  Copyright © 2018 Ramy Nasser. All rights reserved.
//

import UIKit

extension UINavigationBar {
    
    enum NavigationBarStyle {
        case transparent
        case transparentWithShadowLine
        case solid
        case solidNoShadow
        case translucent
    }
    
    // NOTE: color will only be applied to solid and translucent styles
    func setStyle(style : NavigationBarStyle,
                  tintColor : UIColor = .white,
                  forgroundColor: UIColor = .white,
                  backgroundColor : UIColor = .white) {
        switch style {
        case .transparent:
            self.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.shadowImage = UIImage()
            self.isTranslucent = true
            self.backgroundColor = .clear
            self.tintColor = tintColor
        case .transparentWithShadowLine:
            self.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.shadowImage = nil
            self.isTranslucent = true
            self.tintColor = tintColor
            self.backgroundColor = .clear
        case .solid:
            self.setBackgroundImage(nil, for: UIBarMetrics.default)
            self.shadowImage = nil
            self.isTranslucent = false
            self.tintColor = tintColor
            self.barTintColor = forgroundColor
        case .solidNoShadow:
            self.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.shadowImage = UIImage()
            self.isTranslucent = false
            self.tintColor = tintColor
            self.barTintColor = forgroundColor
        case .translucent:
            self.setBackgroundImage(nil, for: UIBarMetrics.default)
            self.shadowImage = nil
            self.isTranslucent = true
            self.tintColor = tintColor
            self.backgroundColor = backgroundColor
            self.barTintColor = forgroundColor
        }
    }
    
    func setTitleColor(color: UIColor) {
        self.titleTextAttributes = [NSAttributedStringKey.foregroundColor : color]
    }
}

