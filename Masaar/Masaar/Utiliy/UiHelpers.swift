//
//  UiHelpers.swift
//  MarvelAPIDemo
//
//  Created by Ramy Nasser  on 7/13/18.
//  Copyright © 2018 Ramy Nasser . All rights reserved.
//

import Foundation
import NVActivityIndicatorView
import SystemConfiguration

class UiHelpers {

    class func showLoader() {
      let activityData = ActivityData()
      NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    class func hideLoader() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    class func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
}
