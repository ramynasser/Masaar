//
//  LocalSettings.swift
//  Masaar
//
//  Created by Ramy Nasser  on 6/28/18.
//  Copyright © 2018 Ramy Nasser . All rights reserved.
//

import Foundation


private let localSettings = LocalSettings()

class LocalSettings {
    // to prevent condition race
    class var instance: LocalSettings {
        return localSettings
    }

    /// - Returns: A String containing the Base URL
    public func getURL() ->String{
        return "https://dhcr.gov.ae/MobileWebAPI/api/Common/ServiceCatalogue"
    }
    
    /// - Returns: A dictionary containing the Base Headers
    public func getHeaders() ->[String : String]{
        return ["Content-Type":"application/json"]
    }
}

