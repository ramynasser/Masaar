//
//  Extension+UIImageView.swift
//  Masaar
//
//  Created by Ramy Nasser  on 1/1/18.
//  Copyright © 2018 Ramy Nasser . All rights reserved.
//

import Foundation
import Foundation
import UIKit
import Localize_Swift
extension UIImageView {
    
    /// function to get the proper height of image relative to the image width of screen
    //then update the constraint of height of UIImageview to the returned value
    
    func getApectRatioHeight()->CGFloat{
        let screenWidth = UIScreen.main.bounds.width
        if self.image == nil {
            return 0
        } else {
            let imageWidth = self.image?.size.width
            let aspectRatio = (screenWidth/imageWidth!) * (self.image?.size.height)!
            return aspectRatio
        }
    }
    
}
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
extension String {
    func formattedDate() ->String{
        var date = self.components(separatedBy: "T")
         return date[0]
    }
   
   var html2AttributedString: NSAttributedString? {
            return Data(utf8).html2AttributedString
    }
    var html2String: String {
            return html2AttributedString?.string ?? ""
    }
}

extension Localize {
    static var isEnglish : Bool {
        return Localize.currentLanguage() == "en"
    }
}
