//
//  GatewayEnums.swift
//  Masaar
//
//  Created by Ramy Nasser Code95 on 7/13/18.
//  Copyright © 2018 Ramy Nasser Code95. All rights reserved.
//

import Foundation

enum NetworkReply {
    case success
    case failure
}

enum StatusCode : Int {
    case badRequest = 400
    case unauthenticated = 401
    case forbidden = 403
    case notFound = 404
    case notAcceptable = 406
    case unsupportedMediaType = 415
    case tooManyRequests = 429
    case serverError = 500
    case unprocessableEntity = 422
    case complete = 200
}
