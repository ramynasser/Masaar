//
//  Storyboards.swift
//  Masaar
//
//  Created by Ramy Nasser  on 7/13/18.
//  Copyright © 2018 Ramy Nasser . All rights reserved.
//

import Foundation
import UIKit

struct Storyboards {
    
    enum Storyboard {
        case Main
    }
    
    static func get(_ storyboard : Storyboard) -> UIStoryboard {
        switch storyboard {
        case .Main:
            return UIStoryboard(name: "Main", bundle: nil)
        }
    }
    
}
